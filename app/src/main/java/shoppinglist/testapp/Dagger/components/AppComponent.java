package shoppinglist.testapp.Dagger.components;

import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import javax.inject.Singleton;
import shoppinglist.testapp.Dagger.ActivityBuilder;
import shoppinglist.testapp.Dagger.FragmentBuilder;
import shoppinglist.testapp.Dagger.module.AppModule;
import shoppinglist.testapp.MyApp;

/**
 * Application Component.
 */
@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        AppModule.class,
        ActivityBuilder.class,
        FragmentBuilder.class})
public interface AppComponent extends AndroidInjector<MyApp> {

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<MyApp> {
    }
}
