package shoppinglist.testapp.Dagger;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import shoppinglist.testapp.Fragments.DialogAddNote.DialogAddNoteFragment;
import shoppinglist.testapp.Fragments.HistoryList.HistoryListFragment;
import shoppinglist.testapp.Fragments.ShoppingList.ShoppingListFragment;

@Module
public abstract class FragmentBuilder {

    @ContributesAndroidInjector
    abstract DialogAddNoteFragment bindDialogAddNoteFragment();

    @ContributesAndroidInjector
    abstract HistoryListFragment bindHistoryListFragment();

    @ContributesAndroidInjector
    abstract ShoppingListFragment bindShoppingListFragment();

}
