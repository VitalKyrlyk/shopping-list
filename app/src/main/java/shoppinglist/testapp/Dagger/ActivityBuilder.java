package shoppinglist.testapp.Dagger;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import shoppinglist.testapp.Activity.MainActivity;

/**
 * Class declare to which activities dependency are injected.
 */
@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector
    abstract MainActivity bindMainActivity();

}
