package shoppinglist.testapp.Dagger.module;

import android.arch.persistence.room.Room;
import android.content.Context;
import dagger.Module;
import dagger.Provides;
import java.util.Objects;
import javax.inject.Singleton;
import shoppinglist.testapp.MyApp;
import shoppinglist.testapp.RoomDB.AppDatabase;
import shoppinglist.testapp.RoomDB.NoteDao;

/**
 * Application Module.
 */
@Module(includes = {NetworkModule.class})
public class AppModule {

    @Provides
    @Singleton
    Context provideContext(MyApp application) {
        return application.getApplicationContext();
    }

    @Provides
    @Singleton
    AppDatabase provideAppDatabase(Context context) {
        return Room.databaseBuilder(Objects.requireNonNull(context),
                AppDatabase.class, "shoppingnote.db").build();
    }

    @Singleton
    @Provides
    public NoteDao provideNoteDao(AppDatabase appDatabase){
        return appDatabase.getNoteDao();
    }
}
