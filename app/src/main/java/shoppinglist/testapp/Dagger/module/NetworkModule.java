package shoppinglist.testapp.Dagger.module;

import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * NNetwork module.
 */
@Module
public class NetworkModule {

    @Provides
    @Singleton
    OkHttpClient provideOkHttp() {
        return new OkHttpClient.Builder().build();
    }

    //TODO: uncomment when start using retrofit 2

//    @Provides
//    @Singleton
//    Retrofit provideRetrofit(OkHttpClient client) {
//        return new Retrofit.Builder()
//                .addConverterFactory(GsonConverterFactory.create())
//                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                .baseUrl(BASE_URL)
//                .client(client)
//                .build();
//    }
//
//    @Provides
//    @Singleton
//    Api provideApi(Retrofit retrofit) {
//        return retrofit.create(Api.class);
//    }
}
