package shoppinglist.testapp.Activity;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import dagger.android.support.DaggerAppCompatActivity;
import shoppinglist.testapp.Fragments.HistoryList.HistoryListFragment;
import shoppinglist.testapp.Fragments.ShoppingList.ShoppingListFragment;
import shoppinglist.testapp.R;

public class MainActivity extends DaggerAppCompatActivity {

    FragmentPagerItemAdapter fragmentPagerItemAdapter;

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @BindView(R.id.viewPagerTabLayout)
    SmartTabLayout smartTabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        fragmentPagerItemAdapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(this)
                .add("Список Покупок", ShoppingListFragment.class)
                .add("История Покупок", HistoryListFragment.class)
                .create());

        viewPager.setAdapter(fragmentPagerItemAdapter);
        smartTabLayout.setViewPager(viewPager);
    }
}
