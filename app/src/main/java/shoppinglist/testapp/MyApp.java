package shoppinglist.testapp;

import android.app.Application;
import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import shoppinglist.testapp.Dagger.components.DaggerAppComponent;

public class MyApp extends DaggerApplication {

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().create(this);
    }

}
