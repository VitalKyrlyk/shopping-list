package shoppinglist.testapp.ImageManger;

public class RequestCodes {
    public static final int IMAGE               = 100;
    public static final int CROP                = 101;
    public static final int GALLERY             = 102;
    public static final int CAMERA              = 103;
    public static final int REQUEST_CODE_PERMISSION_IMAGE_MANAGER              = 104;
}