package shoppinglist.testapp.ImageManger;

public enum AspectRatioConstant {

    RATIO_CUSTOM,
    RATIO_1_1,
    RATIO_4_3,
    RATIO_16_9,

}