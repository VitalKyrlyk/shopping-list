package shoppinglist.testapp.ImageManger;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.util.Log;

import com.soundcloud.android.crop.Crop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Avatar manager for getting image from camera/gallery
 * required to be attached to activity or fragment
 */
public class ImageManager {

    private File mTakePhotoTempFile;
    private String mPhotoPath;
    private Context mCtx;
    private Fragment mFragmentSupport;
    private android.app.Fragment mFragment;
    private Activity mActivity;
    private ImageSizeConstant imageSizeConstant = ImageSizeConstant.IMAGE_DEFAULT_SIZE;
    private AspectRatioConstant aspectRatioConstant = AspectRatioConstant.RATIO_CUSTOM;
    private String authority = "shoppinglist.testapp.provider";

    public ImageManager(Fragment fragment) {
        mFragmentSupport = fragment;
        mCtx = fragment.getActivity();
    }

    public ImageManager(android.app.Fragment fragment) {
        mFragment = fragment;
        mCtx = fragment.getActivity();
    }

    public ImageManager(Activity activity) {
        mActivity = activity;
        mCtx = activity;
    }

    /**
     * set aspect ratio for the crop
     *
     * @param aspectRatioConstant ratio for cropping
     */
    public ImageManager setAspectRatio(AspectRatioConstant aspectRatioConstant) {
        this.aspectRatioConstant = aspectRatioConstant;
        return this;
    }

    /**
     * set image size after crop
     *
     * @param imageSizeConstant size for image
     */
    public ImageManager setImageSize(ImageSizeConstant imageSizeConstant) {
        this.imageSizeConstant = imageSizeConstant;
        return this;
    }

    /**
     * Build intent to perform user crop image
     *
     * @param picUri - image to be cropped
     */
    private void cropImage(Uri picUri) {
        startCropActivity(picUri, RequestCodes.CROP);
        Log.d("myLogs", "Going to crop image with URI = " + picUri);
    }

    /**
     * Init system picker to pick image from camera or gallery
     */
    public void getImage(final String fileName) {
        Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        String MIME_TYPE_IMAGE = "image/*";
        pickIntent.setType(MIME_TYPE_IMAGE);

        Uri uri = null;
        try {
            uri = FileProvider.getUriForFile(mCtx.getApplicationContext(),
                    authority,
                    createImageFile(fileName));
        } catch (IOException ignored) {

        }
        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);

        String CHOOSER_TITLE = "Take or select a picture";
        Intent chooserIntent = Intent.createChooser(pickIntent, CHOOSER_TITLE);
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{takePhotoIntent});

        startActivityForResult(chooserIntent, RequestCodes.IMAGE);
    }

    public void getImageFromGallery() {
        Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        String MIME_TYPE_IMAGE = "image/*";
        pickIntent.setType(MIME_TYPE_IMAGE);
        startActivityForResult(pickIntent, RequestCodes.GALLERY);
    }

    public void getImageFromCamera(final String fileName) {
        Uri uri = null;
        try {
            uri = FileProvider.getUriForFile(mCtx.getApplicationContext(),
                    authority,
                    createImageFile(fileName));
        } catch (IOException ignored) {

        }
        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(takePhotoIntent, RequestCodes.CAMERA);
    }

    /**
     * get image and then rotate it if it needed and start crop activity
     *
     * @param resultCode result code
     * @param data       data which given from intent
     */
    public void handleFullSizeImage(int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            Bitmap bitmap;
            if (data != null && data.getData() != null) {
                Uri selectedPicture = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = mCtx.getContentResolver().query(selectedPicture, filePathColumn, null, null, null);
                assert cursor != null;
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                Bitmap loadedBitmap = BitmapFactory.decodeFile(picturePath);
                ExifInterface exif = null;
                try {
                    File pictureFile = new File(picturePath);
                    exif = new ExifInterface(pictureFile.getAbsolutePath());
                } catch (IOException ignored) {

                }
                int orientation = ExifInterface.ORIENTATION_NORMAL;
                if (exif != null)
                    orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        bitmap = rotateBitmap(loadedBitmap, 90);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        bitmap = rotateBitmap(loadedBitmap, 180);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_270:
                        bitmap = rotateBitmap(loadedBitmap, 270);
                        break;
                    default:
                        bitmap = loadedBitmap;
                        break;
                }
                mTakePhotoTempFile = writeFileFromBitmap(bitmap);
                cropImage(Uri.fromFile(mTakePhotoTempFile));
            } else {
                Bitmap loadedBitmap;
                if (mTakePhotoTempFile != null) {
                    loadedBitmap = getBitmapFromUri(Uri.parse(mTakePhotoTempFile.getAbsolutePath()));
                } else {
                    Uri imageUri = Uri.parse(mPhotoPath);
                    mTakePhotoTempFile = new File(imageUri.getPath());
                    try {
                        InputStream ims = new FileInputStream(mTakePhotoTempFile);
                        loadedBitmap = BitmapFactory.decodeStream(ims);
                    } catch (FileNotFoundException e) {
                        return;
                    }
                }
                ExifInterface ei = null;
                try {
                    ei = new ExifInterface(mTakePhotoTempFile.getAbsolutePath());
                } catch (IOException ignored) {

                }
                assert ei != null;
                int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED);
                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        bitmap = rotateBitmap(loadedBitmap, 90);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        bitmap = rotateBitmap(loadedBitmap, 180);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        bitmap = rotateBitmap(loadedBitmap, 270);
                        break;
                    default:
                        bitmap = loadedBitmap;
                        break;
                }
                mTakePhotoTempFile = writeFileFromBitmap(bitmap);
                cropImage(Uri.fromFile(mTakePhotoTempFile));
            }
        }
    }

    /**
     * @param uri uri of image
     * @return return bitmap of image from uri
     */
    private Bitmap getBitmapFromUri(Uri uri) {
        mCtx.getContentResolver().notifyChange(uri, null);
        ContentResolver cr = mCtx.getContentResolver();
        Bitmap bitmap;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(cr, uri);
            return bitmap;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @param mBitmap bitmap which need to be saved in cache dir
     * @return return .jpg file of bitmap
     */
    private File writeFileFromBitmap(Bitmap mBitmap) {
        boolean fileCreated;
        File f = new File(mCtx.getCacheDir(), "image_" + System.currentTimeMillis() + ".jpg");
        try {
            fileCreated = f.createNewFile();
            Log.i("IMAGE_MANAGER", "file created : = " + fileCreated);
        } catch (IOException ignored) {
        }
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        mBitmap.compress(Bitmap.CompressFormat.JPEG, 100 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (Exception ignored) {
        }
        return f;
    }

    /**
     * Handle cropped image and cache it
     *
     * @return path of cropped image in cache directory
     */
    public String handleCroppedImage(int resultCode) {

        if (resultCode == Activity.RESULT_OK) {
            String absoulutePath = mTakePhotoTempFile.getAbsolutePath();
            mTakePhotoTempFile = null;
            return absoulutePath;
        }
        return null;
    }

    /**
     * Start activity based on root element
     *
     * @param intent      configured intent to start activity
     * @param requestCode request code
     */
    private void startActivityForResult(Intent intent, int requestCode) {
        if (mActivity != null)
            mActivity.startActivityForResult(intent, requestCode);
        else if (mFragment != null)
            mFragment.startActivityForResult(intent, requestCode);
        else if (mFragmentSupport != null)
            mFragmentSupport.startActivityForResult(intent, requestCode);
    }

    /**
     * Start crop activity base on root element and crop image
     *
     * @param picUri      uri of image that need to be cropped
     * @param requestCode request code
     */
    private void startCropActivity(Uri picUri, int requestCode) {
        int weight = getWeight();
        int height = getHeight();
        if (weight == 0 || height == 0) {
            cropWithMaxSize(picUri, requestCode, weight, height);
        } else {
            cropWithCurrentSize(picUri, requestCode);
        }
    }

    private int getHeight() {
        switch (imageSizeConstant) {
            case IMAGE_640_360:
                return 360;
            case IMAGE_854_480:
                return 480;
            case IMAGE_1280_720:
                return 720;
            case IMAGE_1920_1080:
                return 1080;
            case IMAGE_2560_1440:
                return 1440;
            case IMAGE_SQUARE_256:
                return 256;
            case IMAGE_SQUARE_512:
                return 512;
            case IMAGE_SQUARE_640:
                return 640;
            case IMAGE_SQUARE_720:
                return 720;
            case IMAGE_SQUARE_1024:
                return 1024;
            case IMAGE_SQUARE_2048:
                return 2048;
            case IMAGE_DEFAULT_SIZE:
                return 0;
            default:
                return 0;
        }
    }

    private int getWeight() {
        switch (imageSizeConstant) {
            case IMAGE_640_360:
                return 640;
            case IMAGE_854_480:
                return 854;
            case IMAGE_1280_720:
                return 1280;
            case IMAGE_1920_1080:
                return 1920;
            case IMAGE_2560_1440:
                return 2560;
            case IMAGE_SQUARE_256:
                return 256;
            case IMAGE_SQUARE_512:
                return 512;
            case IMAGE_SQUARE_640:
                return 640;
            case IMAGE_SQUARE_720:
                return 720;
            case IMAGE_SQUARE_1024:
                return 1024;
            case IMAGE_SQUARE_2048:
                return 2048;
            case IMAGE_DEFAULT_SIZE:
                return 0;
            default:
                return 0;
        }
    }

    private void cropWithMaxSize(Uri picUri, int requestCode, int weight, int height) {
        switch (aspectRatioConstant) {
            case RATIO_1_1:
                cropSquare(picUri, requestCode, weight, height);
                break;
            case RATIO_4_3:
                cropRectangle(picUri, requestCode, weight, height, 4, 3);
                break;
            case RATIO_16_9:
                cropRectangle(picUri, requestCode, weight, height, 16, 9);
                break;
            case RATIO_CUSTOM:
                crop(picUri, requestCode, weight, height);
                break;
            default:
                crop(picUri, requestCode, weight, height);
                break;
        }
    }

    private void cropWithCurrentSize(Uri picUri, int requestCode) {
        switch (aspectRatioConstant) {
            case RATIO_1_1:
                cropSquare(picUri, requestCode);
                break;
            case RATIO_4_3:
                cropRectangle(picUri, requestCode, 4, 3);
                break;
            case RATIO_16_9:
                cropRectangle(picUri, requestCode, 16, 9);
                break;
            case RATIO_CUSTOM:
                crop(picUri, requestCode);
                break;
            default:
                crop(picUri, requestCode);
                break;
        }
    }

    private void cropSquare(Uri picUri, int requestCode, int weight, int height) {
        if (mActivity != null) {
            Crop.of(picUri, Uri.fromFile(mTakePhotoTempFile)).asSquare().withMaxSize(weight, height).start(mActivity, requestCode);
        } else if (mFragment != null) {
            Crop.of(picUri, Uri.fromFile(mTakePhotoTempFile)).asSquare().withMaxSize(weight, height).start(mFragment.getActivity(), mFragment, requestCode);
        } else if (mFragmentSupport != null) {
            Crop.of(picUri, Uri.fromFile(mTakePhotoTempFile)).asSquare().withMaxSize(weight, height).start(mFragmentSupport.getActivity(), mFragmentSupport, requestCode);
        }
    }

    private void cropSquare(Uri picUri, int requestCode) {
        if (mActivity != null) {
            Crop.of(picUri, Uri.fromFile(mTakePhotoTempFile)).asSquare().start(mActivity, requestCode);
        } else if (mFragment != null) {
            Crop.of(picUri, Uri.fromFile(mTakePhotoTempFile)).asSquare().start(mFragment.getActivity(), mFragment, requestCode);
        } else if (mFragmentSupport != null) {
            Crop.of(picUri, Uri.fromFile(mTakePhotoTempFile)).asSquare().start(mFragmentSupport.getActivity(), mFragmentSupport, requestCode);
        }
    }

    private void cropRectangle(Uri picUri, int requestCode, int weight, int height, int x, int y) {
        if (mActivity != null) {
            Crop.of(picUri, Uri.fromFile(mTakePhotoTempFile)).withAspect(x, y).withMaxSize(weight, height).start(mActivity, requestCode);
        } else if (mFragment != null) {
            Crop.of(picUri, Uri.fromFile(mTakePhotoTempFile)).withAspect(x, y).withMaxSize(weight, height).start(mFragment.getActivity(), mFragment, requestCode);
        } else if (mFragmentSupport != null) {
            Crop.of(picUri, Uri.fromFile(mTakePhotoTempFile)).withAspect(x, y).withMaxSize(weight, height).start(mFragmentSupport.getActivity(), mFragmentSupport, requestCode);
        }
    }

    private void cropRectangle(Uri picUri, int requestCode, int x, int y) {
        if (mActivity != null) {
            Crop.of(picUri, Uri.fromFile(mTakePhotoTempFile)).withAspect(x, y).start(mActivity, requestCode);
        } else if (mFragment != null) {
            Crop.of(picUri, Uri.fromFile(mTakePhotoTempFile)).withAspect(x, y).start(mFragment.getActivity(), mFragment, requestCode);
        } else if (mFragmentSupport != null) {
            Crop.of(picUri, Uri.fromFile(mTakePhotoTempFile)).withAspect(x, y).start(mFragmentSupport.getActivity(), mFragmentSupport, requestCode);
        }
    }

    private void crop(Uri picUri, int requestCode, int weight, int height) {
        if (mActivity != null) {
            Crop.of(picUri, Uri.fromFile(mTakePhotoTempFile)).withMaxSize(weight, height).start(mActivity, requestCode);
        } else if (mFragment != null) {
            Crop.of(picUri, Uri.fromFile(mTakePhotoTempFile)).withMaxSize(weight, height).start(mFragment.getActivity(), mFragment, requestCode);
        } else if (mFragmentSupport != null) {
            Crop.of(picUri, Uri.fromFile(mTakePhotoTempFile)).withMaxSize(weight, height).start(mFragmentSupport.getActivity(), mFragmentSupport, requestCode);
        }
    }

    private void crop(Uri picUri, int requestCode) {
        if (mActivity != null) {
            Crop.of(picUri, Uri.fromFile(mTakePhotoTempFile)).start(mActivity, requestCode);
        } else if (mFragment != null) {
            Crop.of(picUri, Uri.fromFile(mTakePhotoTempFile)).start(mFragment.getActivity(), mFragment, requestCode);
        } else if (mFragmentSupport != null) {
            Crop.of(picUri, Uri.fromFile(mTakePhotoTempFile)).start(mFragmentSupport.getActivity(), mFragmentSupport, requestCode);
        }
    }

    /**
     * @param bitmap  bitmap which need to rotate
     * @param degrees how much degrees we need to rotate needed bitmap
     * @return return rotated bitmap
     */
    private Bitmap rotateBitmap(Bitmap bitmap, int degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    private File createImageFile(String fileName) throws IOException {

        File storageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera");
        File image = File.createTempFile(
                fileName,  /* prefix */
                "",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    public void checkPermissions(int requestCode) {
        if (mActivity != null)
            checkPermissionsActivity(requestCode);
        else if (mFragment != null)
            checkPermissionsFragment(requestCode);
        else if (mFragmentSupport != null)
            checkPermissionsFragmentSupport(requestCode);
    }

    private void checkPermissionsFragment(int requestCode) {
        if (isPermissionsDenied()) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                mFragment.requestPermissions(new String[]{
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                }, requestCode);
        }
    }

    private void checkPermissionsActivity(int requestCode) {
        if (isPermissionsDenied()) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                mActivity.requestPermissions(new String[]{
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                }, requestCode);
        }
    }

    private void checkPermissionsFragmentSupport(int requestCode) {
        if (isPermissionsDenied()) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                mFragmentSupport.requestPermissions(new String[]{
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                }, requestCode);
        }
    }

    public boolean isPermissionsDenied() {
        return ActivityCompat.checkSelfPermission(mCtx, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(mCtx, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(mCtx, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED;
    }

}