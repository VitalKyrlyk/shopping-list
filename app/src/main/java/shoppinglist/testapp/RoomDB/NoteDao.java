package shoppinglist.testapp.RoomDB;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import io.reactivex.Observable;
import io.reactivex.Single;
import java.util.List;

@Dao
public interface NoteDao {

    @Query("SELECT * FROM shoppingnotedata ORDER BY id DESC")
    LiveData<List<ShoppingNoteData>> getAllNotes();

    @Query("SELECT * FROM shoppingnotedata WHERE bought  = 0 ORDER BY id DESC")
    LiveData<List<ShoppingNoteData>> getAllNotBoughtNotes();

    @Query("SELECT * FROM shoppingnotedata WHERE bought  = 1 ORDER BY id DESC")
    LiveData<List<ShoppingNoteData>> getAllBoughtNotes();

    @Query("UPDATE shoppingnotedata SET bought=:bought WHERE id = :id")
    void update(Boolean bought, int id);

    @Insert
    void insertAll(ShoppingNoteData... noteData);

    @Insert
    void insert(ShoppingNoteData noteData);

    @Update
    void update(ShoppingNoteData noteData);

    @Delete
    void delete(ShoppingNoteData noteData);

}
