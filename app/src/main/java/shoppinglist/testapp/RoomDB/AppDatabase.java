package shoppinglist.testapp.RoomDB;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {ShoppingNoteData.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public static final String DATABASE_NAME = "shoppinglist";

    public abstract NoteDao getNoteDao();

}
