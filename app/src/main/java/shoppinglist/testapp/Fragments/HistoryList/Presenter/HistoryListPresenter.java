package shoppinglist.testapp.Fragments.HistoryList.Presenter;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LiveData;
import android.util.Log;

import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import shoppinglist.testapp.Fragments.HistoryList.HistoryListFragment;
import shoppinglist.testapp.Fragments.HistoryList.View.HistoryListView;
import shoppinglist.testapp.Fragments.ShoppingList.Presenter.ShoppingListPresenter;
import shoppinglist.testapp.Fragments.ShoppingList.View.ShoppingListView;
import shoppinglist.testapp.RoomDB.NoteDao;
import shoppinglist.testapp.RoomDB.ShoppingNoteData;

public class HistoryListPresenter {

    private static HistoryListPresenter instance;
    private HistoryListView historyListView;
    private NoteDao noteDao;
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    private HistoryListPresenter(NoteDao noteDao) {
        this.noteDao = noteDao;
    }

    public static HistoryListPresenter getInstance(NoteDao noteDao){
        if (instance == null){
            synchronized (HistoryListPresenter.class){
                if (instance == null){
                    instance = new HistoryListPresenter(noteDao);
                }
            }
        }
        return instance;
    }

    public void getView (HistoryListView view){
        this.historyListView = view;
    }

    public void getNote(LifecycleOwner owner){

        LiveData<List<ShoppingNoteData>> liveData = noteDao.getAllBoughtNotes();

        liveData.observe(owner, shoppingNoteData -> {
            historyListView.getHistoryList(shoppingNoteData);
        });

    }

    public void onDestroy(){
        mCompositeDisposable.dispose();
    }

}
