package shoppinglist.testapp.Fragments.HistoryList.rvAdapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import shoppinglist.testapp.R;
import shoppinglist.testapp.RoomDB.ShoppingNoteData;

public class HistoryListAdapter extends RecyclerView.Adapter<HistoryListAdapter.ViewHolder> {


    private List<ShoppingNoteData>  shoppingNoteData;
    private Context context;

    public HistoryListAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<ShoppingNoteData> shoppingNoteData){
        this.shoppingNoteData = shoppingNoteData;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public HistoryListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.histoty_item, parent, false);
        return new HistoryListAdapter.ViewHolder(view);
    }



    @Override
    public void onBindViewHolder(@NonNull HistoryListAdapter.ViewHolder holder, int position) {

        holder.tvNote.setText(shoppingNoteData.get(position).getNote());
        Glide
                .with(context)
                .load(shoppingNoteData.get(position).getImageUri())
                .into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        if (shoppingNoteData == null){
            return 0;
        } else {
            return shoppingNoteData.size();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvNote;
        ImageView imageView;

        ViewHolder(View itemView) {
            super(itemView);
            tvNote = itemView.findViewById(R.id.tvNoteHistory);
            imageView = itemView.findViewById(R.id.ivNoteHistory);
        }
    }
}
