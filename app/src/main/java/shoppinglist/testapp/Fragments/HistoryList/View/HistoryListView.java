package shoppinglist.testapp.Fragments.HistoryList.View;

import java.util.List;

import shoppinglist.testapp.RoomDB.ShoppingNoteData;

public interface HistoryListView {
    void getHistoryList(List<ShoppingNoteData> shoppingNoteData);
}
