package shoppinglist.testapp.Fragments.HistoryList;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;
import shoppinglist.testapp.Fragments.HistoryList.Presenter.HistoryListPresenter;
import shoppinglist.testapp.Fragments.HistoryList.View.HistoryListView;
import shoppinglist.testapp.Fragments.HistoryList.rvAdapter.HistoryListAdapter;
import shoppinglist.testapp.R;
import shoppinglist.testapp.RoomDB.NoteDao;
import shoppinglist.testapp.RoomDB.ShoppingNoteData;

public class HistoryListFragment extends DaggerFragment implements HistoryListView {

    View view;
    HistoryListAdapter adapter;
    HistoryListPresenter presenter;

    @Inject
    protected NoteDao noteDao;

    @BindView(R.id.rvHistoryList)
    RecyclerView recyclerView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("InflateParams")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.history_list_fragment, null);
        ButterKnife.bind(this, view);

        presenter = HistoryListPresenter.getInstance(noteDao);
        presenter.getView(this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        adapter = new HistoryListAdapter(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        presenter.getNote(this);


        return view;
    }

    @Override
    public void getHistoryList(List<ShoppingNoteData> shoppingNoteData) {
        adapter.setData(shoppingNoteData);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }
}
