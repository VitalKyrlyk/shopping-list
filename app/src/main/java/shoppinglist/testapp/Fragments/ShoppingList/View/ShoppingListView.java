package shoppinglist.testapp.Fragments.ShoppingList.View;

import java.util.List;

import shoppinglist.testapp.RoomDB.ShoppingNoteData;

public interface ShoppingListView {

    void getShoppingList(List<ShoppingNoteData> shoppingNoteData);
}
