package shoppinglist.testapp.Fragments.ShoppingList.Presenter;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LiveData;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import shoppinglist.testapp.Fragments.ShoppingList.View.ShoppingListView;
import shoppinglist.testapp.RoomDB.NoteDao;
import shoppinglist.testapp.RoomDB.ShoppingNoteData;

public class ShoppingListPresenter {

    private static ShoppingListPresenter instance;
    private ShoppingListView shoppingListView;
    private NoteDao noteDao;
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    private ShoppingListPresenter(NoteDao noteDao) {
        this.noteDao = noteDao;
    }

    public static ShoppingListPresenter getInstance(NoteDao noteDao){
        if (instance == null){
            synchronized (ShoppingListPresenter.class){
                if (instance == null){
                    instance = new ShoppingListPresenter(noteDao);
                }
            }
        }
        return instance;
    }

    public void getView (ShoppingListView view){
        this.shoppingListView = view;
    }

    public void getNote(LifecycleOwner owner){

        LiveData<List<ShoppingNoteData>> liveData = noteDao.getAllNotBoughtNotes();
        liveData.observe(owner, shoppingNoteData -> {
            shoppingListView.getShoppingList(shoppingNoteData);
        });

    }

    public void onDestroy(){
        mCompositeDisposable.dispose();
    }

}
