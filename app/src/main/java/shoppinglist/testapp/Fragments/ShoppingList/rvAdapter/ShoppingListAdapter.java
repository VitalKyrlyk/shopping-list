package shoppinglist.testapp.Fragments.ShoppingList.rvAdapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import shoppinglist.testapp.Fragments.ShoppingList.ShoppingListFragment;
import shoppinglist.testapp.R;
import shoppinglist.testapp.RoomDB.ShoppingNoteData;

public class ShoppingListAdapter extends RecyclerView.Adapter<ShoppingListAdapter.ViewHolder> {

    private List<ShoppingNoteData> shoppingNoteData;
    private Context context;
    private ShoppingListFragment shoppingListFragment;

    public ShoppingListAdapter(Context context, ShoppingListFragment shoppingListFragment) {
        this.context = context;
        this.shoppingListFragment = shoppingListFragment;
    }

    public void setData(List<ShoppingNoteData> shoppingNoteData){
        this.shoppingNoteData = shoppingNoteData;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shopping_item, parent, false);
        return new ShoppingListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ShoppingListAdapter.ViewHolder holder, int position) {

        holder.tvNote.setText(shoppingNoteData.get(position).getNote());
        Glide
                .with(context)
                .load(shoppingNoteData.get(position).getImageUri())
                .into(holder.imageView);

        if (shoppingNoteData.get(position).getBought()){
            holder.cbSelect.setChecked(true);
        } else {
            holder.cbSelect.setChecked(false);
        }
        holder.cbSelect.setOnCheckedChangeListener((compoundButton, b) -> {
            shoppingListFragment.setVisibilityButton();
            if (holder.cbSelect.isChecked()){
            shoppingListFragment.data.get(position).setBought(true);
            } else {
                shoppingListFragment.data.get(position).setBought(false);
            }
        });
    }


    @Override
    public int getItemCount() {
        if (shoppingNoteData == null){
            return 0;
        } else {
            return shoppingNoteData.size();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvNote;
        CheckBox cbSelect;
        ImageView imageView;
        View view;

        ViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            tvNote = itemView.findViewById(R.id.tvNoteShopping);
            cbSelect = itemView.findViewById(R.id.cbSelect);
            imageView = itemView.findViewById(R.id.ivNoteShopping);
        }
    }
}
