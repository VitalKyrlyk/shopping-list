package shoppinglist.testapp.Fragments.ShoppingList;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import dagger.android.support.DaggerFragment;
import shoppinglist.testapp.Fragments.DialogAddNote.DialogAddNoteFragment;
import shoppinglist.testapp.Fragments.ShoppingList.Presenter.ShoppingListPresenter;
import shoppinglist.testapp.Fragments.ShoppingList.View.ShoppingListView;
import shoppinglist.testapp.Fragments.ShoppingList.rvAdapter.ShoppingListAdapter;
import shoppinglist.testapp.R;
import shoppinglist.testapp.RoomDB.NoteDao;
import shoppinglist.testapp.RoomDB.ShoppingNoteData;

public class ShoppingListFragment extends DaggerFragment implements ShoppingListView {

    View view;
    ShoppingListPresenter presenter;
    ShoppingListAdapter adapter;
    public List<ShoppingNoteData> data;

    @Inject
    protected NoteDao mNoteDao;

    @BindView(R.id.rvShoppingList)
    RecyclerView recyclerView;

    @BindView(R.id.addNoteBnt)
    FloatingActionButton actionButton;

    @BindView(R.id.cbSelectAll)
    CheckBox cbSelectAll;

    @OnCheckedChanged(R.id.cbSelectAll)
    public void SelectAll() {
        if (cbSelectAll.isChecked()) {
            for (int i = 0; i < data.size(); i++) {
                data.get(i).setBought(true);
                adapter.setData(data);
            }
        } else {
            for (int i = 0; i < data.size(); i++) {
                data.get(i).setBought(false);
                adapter.setData(data);
            }
        }
    }

    @BindView(R.id.btnAddToHistory)
    Button btnAddToHistory;

    @OnClick(R.id.btnAddToHistory)
    public void AddToHistory(){
        for (int i = 0; i < data.size(); i++){
            if (data.get(i).getBought()){
                int finalI = i;
                AsyncTask.execute(() -> mNoteDao.update(true, data.get(finalI).getId()));
            }
        }
        cbSelectAll.setChecked(false);
        btnAddToHistory.setVisibility(View.INVISIBLE);
        presenter.getNote(this);
    }

    @OnClick({R.id.addNoteBnt})
    public void AddNote(){
        DialogAddNoteFragment dialogAddNoteFragment = new DialogAddNoteFragment();
        assert getFragmentManager() != null;
        dialogAddNoteFragment.show(getFragmentManager(), "Dialog");
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("InflateParams")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.shopping_list_fragment, null);
        ButterKnife.bind(this, view);

        presenter = ShoppingListPresenter.getInstance(mNoteDao);
        presenter.getView(this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        adapter = new ShoppingListAdapter(getActivity(), this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        presenter.getNote(this);

        return view;
    }

    public void setVisibilityButton(){
        btnAddToHistory.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void getShoppingList(List<ShoppingNoteData> shoppingNoteData) {
        adapter.setData(shoppingNoteData);
        this.data = shoppingNoteData;
    }
}
