package shoppinglist.testapp.Fragments.DialogAddNote;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatDialogFragment;
import shoppinglist.testapp.Fragments.DialogAddNote.Presenter.DialogAddNotePresenter;
import shoppinglist.testapp.Fragments.DialogAddNote.View.DialogAddNoteView;
import shoppinglist.testapp.ImageManger.AspectRatioConstant;
import shoppinglist.testapp.ImageManger.ImageManager;
import shoppinglist.testapp.ImageManger.ImageSizeConstant;
import shoppinglist.testapp.ImageManger.RequestCodes;
import shoppinglist.testapp.R;
import shoppinglist.testapp.RoomDB.NoteDao;

import static android.app.Activity.RESULT_OK;

public class DialogAddNoteFragment extends DaggerAppCompatDialogFragment implements DialogAddNoteView{

    DialogAddNotePresenter addNotePresenter;
    ImageManager imageManager;
    String imageUri;

    @Inject
    protected NoteDao mNoteDao;

    @BindView(R.id.etAddNote)
    EditText etAddNote;

    @BindView(R.id.btnAddImage)
    Button btnAddNote;

    @OnClick({R.id.btnAddImage})
    public void AddImage(){
        if (imageManager.isPermissionsDenied())
            imageManager.checkPermissions(RequestCodes.REQUEST_CODE_PERMISSION_IMAGE_MANAGER);
        else
            imageManager.getImage("file_name");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));

        LayoutInflater inflater = getActivity().getLayoutInflater();
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.dialog_add_note, null);
        ButterKnife.bind(this, view);

        addNotePresenter = DialogAddNotePresenter.getInstance(mNoteDao);
        addNotePresenter.getView(this);

        imageManager = new ImageManager(this);

        builder.setView(view)
                .setTitle(R.string.add_new)
                .setNegativeButton("Назад", (dialogInterface, i) -> {

                })
                .setPositiveButton("Добавить", (dialogInterface, i) -> {
                            addNotePresenter.addNote(etAddNote.getText().toString(), imageUri);
                });

        return builder.create();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK)
            if (requestCode == RequestCodes.IMAGE) {
                imageManager
                        .setImageSize(ImageSizeConstant.IMAGE_DEFAULT_SIZE)
                        .setAspectRatio(AspectRatioConstant.RATIO_CUSTOM)
                        .handleFullSizeImage(resultCode, data);
            } else if (requestCode == RequestCodes.CROP){
            imageUri = imageManager.handleCroppedImage(resultCode);
            }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == RequestCodes.REQUEST_CODE_PERMISSION_IMAGE_MANAGER) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            }
        }
    }


    @Override
    public void emptyBodyError() {
        Toast.makeText(getContext(), R.string.empty_error, Toast.LENGTH_SHORT).show();
    }

}
