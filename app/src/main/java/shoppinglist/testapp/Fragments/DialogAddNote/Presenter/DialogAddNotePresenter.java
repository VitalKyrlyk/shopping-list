package shoppinglist.testapp.Fragments.DialogAddNote.Presenter;

import android.os.AsyncTask;

import shoppinglist.testapp.Fragments.DialogAddNote.View.DialogAddNoteView;
import shoppinglist.testapp.RoomDB.NoteDao;
import shoppinglist.testapp.RoomDB.ShoppingNoteData;

public class DialogAddNotePresenter {

    private static DialogAddNotePresenter instance;
    private NoteDao mNoteDao;
    private DialogAddNoteView dialogAddNoteView;

    private DialogAddNotePresenter(NoteDao noteDao) {
        mNoteDao = noteDao;
    }

    public static DialogAddNotePresenter getInstance(NoteDao noteDao){
        if (instance == null){
            synchronized (DialogAddNotePresenter.class){
                if (instance == null) {
                    instance = new DialogAddNotePresenter(noteDao);
                }
            }
        }
        return instance;
    }

    public void getView (DialogAddNoteView view){
        this.dialogAddNoteView = view;
    }

    public void addNote(String note, String imageUri){
        if (note.isEmpty()){
            if (imageUri == null){
                dialogAddNoteView.emptyBodyError();
            } else {
                saveNote(note, imageUri);
            }
        } else {
            saveNote(note, imageUri);
        }
    }

    private void saveNote(String note, String imageUri){
        ShoppingNoteData data = new ShoppingNoteData();
        data.setNote(note);
        data.setImageUri(imageUri);
        data.setBought(false);
        AsyncTask.execute(() -> mNoteDao.insert(data));
    }
}
